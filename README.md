# Hip Yo Chat

Atlassian Connect Addon for Yo.

Connect this addon with your hipchat room, enter the API key from Yo on the config screen.

Type /yo to Yo all your followers.
Get notifications when someone Yo's your handle