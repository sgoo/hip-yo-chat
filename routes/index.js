var http = require('request');
var pmongo = require('promised-mongo');

module.exports = function (app, addon) {
    var hipchat = require('../lib/hipchat')(addon);
    var addonSettings = pmongo(addon.config.store().url).collection('AddonSettings');

    app.get('/', function(req, res) {
        res.redirect('/atlassian-connect.json');
    });

    // There is no way to associate incoming yo's with different rooms.
    // So we Yo all rooms because #yolo
    app.get('/cb', function (req, res) {
        var user = req.query.username
        addonSettings.find({
            key: 'clientInfo'
        }).toArray().then(function(clientInfos) {
            clientInfos.forEach(function(dbObject) {
                var clientInfo = dbObject.val;
                hipchat.sendMessage(clientInfo, clientInfo.roomId, user + ": Yo");
            });
        });
        res.send(200);
    });

    app.get('/config', addon.authenticate(), function(req, res) {
        addon.settings.get('ApiKey', req.clientInfo.clientKey).then(function(apiKey) {
            req.context.apiKey = apiKey
            res.render('config', req.context);
        });
    });

    app.post('/config', addon.authenticate(), function(req, res) {
        var apiKey = req.body.apiKey || '';
        addon.settings.set('ApiKey', apiKey, req.clientInfo.clientKey).then(function() {
            req.context.apiKey = apiKey
            req.context.success = true;
            res.render('config', req.context);
        });
    });

    app.post('/webhook', addon.authenticate(), function(req, res) {
        if (req.body.item.message.message === '/yo') {
            addon.settings.get('ApiKey', req.clientInfo.clientKey).then(function(apiKey) {
                http({
                    url: 'http://api.justyo.co/yoall/',
                    method: 'POST',
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    body: 'api_token=' + apiKey
                }, function(err, yoRes, yoBody) {
                    console.log(yoBody);
                    if (err) {
                        console.error("YOALL Failed!!!");
                        res.send(500);
                    } else {
                        res.send(200);
                    }
                });
            });
        }
    });

    // Notify the room that the add-on was installed
    addon.on('installed', function(clientKey, clientInfo, req){
        hipchat.sendMessage(clientInfo, req.body.roomId, '' + addon.descriptor.name + ' has been installed in this room');
    });

    // Clean up clients when uninstalled
    addon.on('uninstalled', function(id){
        addon.settings.client.keys(id+':*', function(err, rep){
            rep.forEach(function(k){
                addon.logger.info('Removing key:', k);
                addon.settings.client.del(k);
            });
        });
    });

};
